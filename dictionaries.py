# exercise 7

Friend = {
    'name' : 'Jas Fasola',
    'age' : '35',
    'hobbies' : ['sport', 'bike'],
}

print(Friend)

print(Friend['age'])
Friend['city'] = 'Wroclaw'

print(Friend)

Friend['job'] = 'doctor'
print(Friend)
del Friend['job']
print(Friend)
Friend['hobbies'].append('climbing')
print(Friend)

# lista exercise

