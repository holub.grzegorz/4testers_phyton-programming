
# Exercise 1
def get_number_squared(num):
    return num ** 2

zero_squared = get_number_squared(0)
print(zero_squared)

sixteen_squared = get_number_squared(16)
print(sixteen_squared)

floating_number_squared = get_number_squared(2.55)
print(floating_number_squared)

def get_cuboid_volume(a, b, c):
    return a * b * c

print(get_cuboid_volume(3, 5, 7))


# Exercise 3

def convert_cels_to_farenh(degrees_cels) :
    return 32 + 9/5 * degrees_cels
print (convert_cels_to_farenh(20))

Name = "Michal"
City = "Torun"

# exercise 1 print welcome message to a person in a city

def print_hello_message(name, city):
    name_capitalized = name.capitalize()
    city_capitalized = city.capitalize()
    print(f"Witaj {name.capitalize()}! Milo Cie widziec w naszym miescie: {city.capitalize()}!")

print_hello_message("Michal", "Torun")
print_hello_message("Beata", "Gdynia")
print_hello_message("adam", "krakow")

# Exercise 2

def get_email(name, surname):
    return f'{name.lower()}.{surname.lower()}@4testers.com'

print(get_email("Janusz", "Nowak"))
print(get_email("barbara", "kowalska"))


employee_1_first_name = "Barbara"
employee_1_last_name = "Nowak"
employee_1_email = f"{employee_1_first_name.lower()}.{employee_1_last_name.lower()}@4testers.com"

employee_2_first_name = "Janusz"
employee_2_last_name = "Kowalski"
employee_2_email = f"{employee_2_first_name.lower()}.{employee_2_last_name.lower()}@4testers.com"
employee_3_first_name = "Jas"
employee_3_last_name = "Fasola"
employee_3_email = f"{employee_3_first_name.lower()}.{employee_3_last_name.lower()}@4testers.com"

print(employee_1_email)
print(employee_2_email)
print(employee_3_email)

